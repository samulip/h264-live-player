"use strict";


const WebSocketServer = require('ws').Server;
const Splitter        = require('stream-split');
const merge           = require('mout/object/merge');

const NALseparator    = new Buffer([0,0,0,1]);//NAL break


class _Server {

  constructor(server, options) {

    this.options = merge({
        width : 960,
        height: 540,
    }, options);

    this.wss = new WebSocketServer({ server });

    this.new_client = this.new_client.bind(this);
    this.start_feed = this.start_feed.bind(this);
    this.broadcast  = this.broadcast.bind(this);

    this.wss.on('connection', this.new_client);
  }


  start_feed(options) {
    var readStream = this.get_feed(options);
    this.readStream = readStream;

    readStream = readStream.pipe(new Splitter(NALseparator));
    readStream.on("data", this.broadcast);
    readStream.on("error", (err) => console.log("ERR:", err));
    readStream.on("close", function () {
      console.log("_server.js close");
    });
  }

  get_feed() {
    throw new Error("to be implemented");
  }

  broadcast(data) {
    this.wss.clients.forEach(function(socket) {

      if(socket.buzy)
        return;

      socket.buzy = true;
      socket.buzy = false;

      socket.send(Buffer.concat([NALseparator, data]), { binary: true}, function ack(error) {
        socket.buzy = false;
      });
    });
  }

  new_client(socket) {

    var self = this;
    console.log('Client connected');

    socket.on("message", function(data){
      var cmd = "" + data, action = data.split(' ')[0], opts = JSON.parse((data.split(' ').slice(1) || '{}'));
      console.log("> Message '%s'", action, opts);

      if (action == "STOPSTREAM" || (action == "REQUESTSTREAM" && !!self.streamer)) {
        if (self.readStream) self.readStream.destroy();
        self.readStream = undefined;
        console.log("Stream closed");
      }

      if (action == "REQUESTSTREAM") {
        if (self.readStream) {
          self.readStream.destroy();
  };
/*
        socket.send(JSON.stringify({
          action : "init",
          width  : opts.width,
          height : opts.height,
        }));
*/
        self.start_feed(opts);
        console.log("Stream opened");
      }
    });

    socket.on('close', function() {
      if(self.readStream) {
        self.readStream.destroy();
        self.readStream = undefined;
      }
      console.log('Client disconnected');
    });
  }


};


module.exports = _Server;
